#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>
#include <time.h>
#include <windows.h>
#include "Header.h"

static int brojUsera = 0;
static USER* pronadeniUser = NULL;
static USER* poljeUsera = NULL;
void dodajUsera() {
    FILE* pF = fopen("snake.bin", "rb+");
    if (pF == NULL) {
        perror("Dodavanje Usera u datoteke snake.bin");
        exit(EXIT_FAILURE);
    }
    fread(&brojUsera, sizeof(int), 1, pF);
    printf("broj Usera: %d\n", brojUsera);
    USER temp = { 0 };
    getchar();
    printf("Unesite ime usera\n");
    scanf("%19[^\n]", temp.username);
    fseek(pF, sizeof(USER) * brojUsera, SEEK_CUR);
    fwrite(&temp, sizeof(USER), 1, pF);
    rewind(pF);
    brojUsera++;
    fwrite(&brojUsera, sizeof(int), 1, pF);
    fclose(pF);
}

void* ucitavanjeUsera() {
    FILE* pF = fopen("snake.bin", "rb");
    if (pF == NULL) {
        perror("Ucitavanje studenata iz datoteke studenti.bin");
        return NULL;
    }
    fread(&brojUsera, sizeof(int), 1, pF);
    printf("broj usera: %d\n", brojUsera);
    USER* poljeUsera = (USER*)calloc(brojUsera, sizeof(USER));
    if (poljeUsera == NULL) {
        perror("Zauzimanje memorije za usere");
        return NULL;
    }
    fread(poljeUsera, sizeof(USER), brojUsera, pF);
    fclose(pF);
    return poljeUsera;

}

void ispisUsera(const USER* const poljeUsera) {
    if (poljeUsera == NULL) {
        printf("Polje Usera je prazno!\n");
        return;
    }
    int i;
    for (i = 0; i < brojUsera; i++)
    {
        printf("User broj %d\tIme: %s\t\n", i + 1, (poljeUsera + i)->username);
    }
}


void* pretrazivanjeUsera(USER* const poljeUsera) {
    if (poljeUsera == NULL) {
        printf("Polje studenata je prazno!\n");
        return NULL;
    }
    int i;
    char trazenoIme[20] = { '\0' };
        printf("Unesite trazeni kriterij za pronalazak usera.\n");
        getchar();
        scanf("%19[^\n]", trazenoIme);
        for (i = 0; i < brojUsera; i++)
        {
            if (!strcmp(trazenoIme, (poljeUsera + i)->username)) {
                printf("User je pronaden!\n");
                return (poljeUsera + i);
            }
          return NULL;
        }
}
          

void brisanjeUsera(USER** const trazeniUser, const USER* const poljeUsera) {
    FILE* pF = fopen("snake.bin", "wb");
    if (pF == NULL) {
        perror("Brisanje usera iz datoteke snake.bin");
        exit(EXIT_FAILURE);
    }
    fseek(pF, sizeof(int), SEEK_SET);
    int i;
    int noviBrojacUsera = 0;
    for (i = 0; i < brojUsera; i++)
    {
        if (*trazeniUser != (poljeUsera + i)) {
            fwrite((poljeUsera + i), sizeof(USER), 1, pF);
            noviBrojacUsera++;
        }
    }
    rewind(pF);
    fwrite(&noviBrojacUsera, sizeof(int), 1, pF);
    fclose(pF);
    printf("User je uspjesno obrisan!\n");
    *trazeniUser = NULL;
}

void brisanjeDatoteke() {
    int brisanje = 1;
    printf("Zelite li uistinu obrisati datoteku\n");
    printf("Utipkajte \"da\" ako uistinu zelite obrisati datoteku u suprotno utipkajte\
\"ne\"!\n");
    char potvrda[3] = { '\0' };
    scanf("%2s", potvrda);

    if (!strcmp("da", potvrda)) {
        brisanje = remove("snake.bin");

    }
    if (brisanje == 0) {
        printf("Datoteka uspijesno obrisana\n");
    }
    else {
        //perror("Error: ");
        printf("Brisanje datoteke neuspjesno\n");
    }
}

int izlazIzPrograma(USER* poljeUsera) {
    free(poljeUsera);
    return 0;
}



void kreiranjeDatoteke() {
    FILE* pF = fopen("snake.bin", "ab");
    if (pF == NULL) {
        perror("Kreiranje dat");
        exit(EXIT_FAILURE);
    }
    fwrite(&brojUsera, sizeof(int), 1, pF);
    fclose(pF);

}


#define UP 72
#define DOWN 80
#define LEFT 75
#define RIGHT 77
int score;
int length;
int bend_no;
int len;
char key;
int life;
void Delay(long double);
void Move();
void Food();
int Score();
void gotoxy(int x, int y);
void GotoXY(int x, int y);
void Bend();
void Boarder();
void Down();
void Left();
void Up();
void Right();
void ExitGame();


struct coordinate
{
    int x;
    int y;
    int direction;
};

typedef struct coordinate coordinate;

coordinate head, bend[500], food, body[30];

int snake()
{

    char key;


    system("cls");


    length = 5;

    head.x = 25;

    head.y = 20;

    head.direction = RIGHT;

    Boarder();

    Food(); //stvaranje voca

    life = 0;//zivot

    bend[0] = head;

    Move();   //pocetni koordinati savijanja

    return 0;

}

void Move()
{
    int a, i;

    do
    {

        Food();
        fflush(stdin);

        len = 0;

        for (i = 0; i < 30; i++)

        {

            body[i].x = 0;

            body[i].y = 0;

            if (i == length)

                break;

        }

        Delay(length);

        Boarder();

        if (head.direction == RIGHT)

            Right();

        else if (head.direction == LEFT)

            Left();

        else if (head.direction == DOWN)

            Down();

        else if (head.direction == UP)

            Up();

        ExitGame();

    } while (!_kbhit());

    a = _getch();

    if (a == 27)

    {

        system("cls");

        exit(0);

    }
    key = _getch();

    if ((key == RIGHT && head.direction != LEFT && head.direction != RIGHT) || (key == LEFT && head.direction != RIGHT && head.direction != LEFT) || (key == UP && head.direction != DOWN && head.direction != UP) || (key == DOWN && head.direction != UP && head.direction != DOWN))

    {

        bend_no++;

        bend[bend_no] = head;

        head.direction = key;

        if (key == UP)

            head.y--;

        if (key == DOWN)

            head.y++;

        if (key == RIGHT)

            head.x++;

        if (key == LEFT)

            head.x--;

        Move();

    }

    else if (key == 27)

    {

        system("cls");

        exit(0);

    }

    else

    {

        printf("\a");

        Move();

    }
}

void gotoxy(int x, int y)
{

    COORD coord;

    coord.X = x;

    coord.Y = y;

    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);

}
void GotoXY(int x, int y)
{
    HANDLE a;
    COORD b;
    fflush(stdout);
    b.X = x;
    b.Y = y;
    a = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleCursorPosition(a, b);
}


void Down()
{
    int i;
    for (i = 0; i <= (head.y - bend[bend_no].y) && len < length; i++)
    {
        GotoXY(head.x, head.y - i);
        {
            if (len == 0)
                printf("v");
            else
                printf("*");
        }
        body[len].x = head.x;
        body[len].y = head.y - i;
        len++;
    }
    Bend();
    if (!_kbhit())
        head.y++;
}
void Delay(long double k)
{
    Score();
    long double i;
    for (i = 0; i <= (10000000); i++);
}
void ExitGame()
{
    int i, check = 0;
    for (i = 4; i < length; i++) //min 4 potrebno za dodir tijela
    {
        if (body[0].x == body[i].x && body[0].y == body[i].y)
        {
            check++;    //povecava se ako su koordinati glave jednaki nekom dijelu tijela                                                                                                                                                                                          
        }
        if (i == length || check != 0)
            break;
    }
    if (head.x <= 10 || head.x >= 70 || head.y <= 10 || head.y >= 30 || check != 0)                                                 
    {
        life--;
        if (life >= 0)
        {
            head.x = 25;
            head.y = 20;
            bend_no = 0;
            head.direction = RIGHT;
            Move();
        }
        else
        {
            system("cls");
            exit(0);
        }
    }
}
void Food()
{
    if (head.x == food.x && head.y == food.y)
    {
        length++;
        time_t a;
        a = time(0);
        srand(a);
        food.x = rand() % 70;
        if (food.x <= 10)
            food.x += 11;
        food.y = rand() % 30;
        if (food.y <= 10)

            food.y += 11;
    }
    else if (food.x == 0)//kako bi prvi put stvorilo voce jer je inncijalizirano s 0
    {
        food.x = rand() % 70;
        if (food.x <= 10)
            food.x += 11;
        food.y = rand() % 30;
        if (food.y <= 10)
            food.y += 11;
    }
}
void Left()
{
    int i;
    for (i = 0; i <= (bend[bend_no].x - head.x) && len < length; i++)
    {
        GotoXY((head.x + i), head.y);
        {
            if (len == 0)
                printf("<");
            else
                printf("*");
        }
        body[len].x = head.x + i;
        body[len].y = head.y;
        len++;
    }
    Bend();
    if (!_kbhit())
        head.x--;

}
void Right()
{
    int i;
    for (i = 0; i <= (head.x - bend[bend_no].x) && len < length; i++)
    {
        
        body[len].x = head.x - i;
        body[len].y = head.y;
        GotoXY(body[len].x, body[len].y);
        {
            if (len == 0)
                printf(">");
            else
                printf("*");
        }
        
        len++;
    }
    Bend();
    if (!_kbhit())
        head.x++;
}
void Bend()
{
    int i, j, diff;
    for (i = bend_no; i >= 0 && len < length; i--)
    {
        if (bend[i].x == bend[i - 1].x)
        {
            diff = bend[i].y - bend[i - 1].y;
            if (diff < 0)
                for (j = 1; j <= (-diff); j++)
                {
                    body[len].x = bend[i].x;
                    body[len].y = bend[i].y + j;
                    GotoXY(body[len].x, body[len].y);
                    printf("*");
                    len++;
                    if (len == length)
                        break;
                }
            else if (diff > 0)
                for (j = 1; j <= diff; j++)
                {
                  
                    body[len].x = bend[i].x;
                    body[len].y = bend[i].y - j;
                    GotoXY(body[len].x, body[len].y);
                    printf("*");
                    len++;
                    if (len == length)
                        break;
                }
        }
        else if (bend[i].y == bend[i - 1].y)
        {
            diff = bend[i].x - bend[i - 1].x;
            if (diff < 0)
                for (j = 1; j <= (-diff) && len < length; j++)
                {
                   
                    body[len].x = bend[i].x + j;
                    body[len].y = bend[i].y;
                    GotoXY(body[len].x, body[len].y);
                    printf("*");
                    len++;
                    if (len == length)
                        break;
                }
            else if (diff > 0)
                for (j = 1; j <= diff && len < length; j++)
                {
                   
                    body[len].x = bend[i].x - j;
                    body[len].y = bend[i].y;
                    GotoXY(body[len].x, body[len].y);
                    printf("*");
                    len++;
                    if (len == length)
                        break;
                }
        }
    }
}
void Boarder()
{
    system("cls");
    int i;
    GotoXY(food.x, food.y);  
    printf("O");
    for (i = 10; i < 71; i++)
    {
        GotoXY(i, 10);
        printf("!");
        GotoXY(i, 30);
        printf("!");
    }
    for (i = 10; i < 31; i++)
    {
        GotoXY(10, i);
        printf("!");
        GotoXY(70, i);
        printf("!");
    }
}

int Score()
{
    GotoXY(20, 8);
    score = 0;
    printf("SCORE:%d", length - 5);
    score = length - 5;
    GotoXY(50, 8);
    
    return score;
}

void Up()
{
    int i;
    for (i = 0; i <= (bend[bend_no].y - head.y) && len < length; i++)
    {
        GotoXY(head.x, head.y + i);
        {
            if (len == 0)
                printf("^");
            else
                printf("*");
        }
        body[len].x = head.x;
        body[len].y = head.y + i;
        len++;
    }
    Bend();
    if (!_kbhit())
        head.y--;
}

int izbornik() {

    printf("Odberite jednu od ponudenih opcija\n");
    printf("1:Dodaj usera\n");
    printf("2:Ucitavanje usera\n");
    printf("3:Ispis postojecih usera\n");
    printf("4:Pretrazivanje usera\n");
    printf("5:Brisanje usera\n");
    printf("6:Brisanje datoteke\n");
    printf("7:Izlaz iz programa\n");
    printf("8:Pokreni igru\n");

    int uvijet = 0;
    scanf("%d", &uvijet);
    switch (uvijet) {
    case 1:
        dodajUsera();
        break;
    case 2:
        if (poljeUsera != NULL) {
            free(poljeUsera);
            poljeUsera = NULL;
        }
        poljeUsera = (USER*)ucitavanjeUsera();
        if (poljeUsera == NULL) {
            exit(EXIT_FAILURE);
        }
        break;
    case 3:
        ispisUsera(poljeUsera);
        break;
    case 4:
        pronadeniUser = (USER*)pretrazivanjeUsera(poljeUsera);
        break;
    case 5:
        brisanjeUsera(&pronadeniUser, poljeUsera);
        break;
    case 6:
        brisanjeDatoteke();
        break;
    case 7:
        uvijet = izlazIzPrograma(poljeUsera);
        break;
    case 8:
        snake(poljeUsera);
        break;
    default:
        uvijet = 0;
    }

    return uvijet;
}


