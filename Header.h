#ifndef HEADER_H
#define HEADER_H

typedef struct user {
	char username[20];
}USER;

int izbornik();
void kreiranjeDatoteke();
void dodajUsera();
void ispisUsera(const USER*const);
void ispisHighscore(const USER* const);
void* ucitavanjeUsera();
void* pretrazivanjeUsera(USER* const);
void brisanjeUsera(USER** const, const USER* const);
void brisanjeDatoteke();
int izlazIzPrograma(USER*);
#endif